# Sass

As covered in the [Sass folder structure](/folder-structure/sass-folder-structure.md) all Sass files should be in `/src/sass/`

The Gulp.js build system automatically compiles the Sass into CSS using [Node Sass](https://github.com/sass/node-sass). The CSS files are compiled to the `/assets/css/` folder.

Many [Sass and PostCSS plugins](/build-system/included-libraries.md) are included to help with theming.

One of the most important and useful is [autoprefixer](https://github.com/postcss/autoprefixer) which automatically adds any browser CSS vendor prefixes based on the browserslist config in the theme's `package.json` file. This means that prefixes **_should not_** be added in Sass files. _They will be added to the CSS files when they are compiled._
