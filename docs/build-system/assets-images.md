# Images

The theme's asset build system includes many plugins and tasks for loading and optimizing image assets.

All theme related images should be placed in the `/src/images/` folder.

> [!TIP] It is _highly_ recommended to organize the `/src/images/` folder further into sub-folders. Two common sub-folders for themes is one for `icons`, and one for `logos`.

## Image Optimization

Images will be copied to the `/assets/images/` folder. Before copying, all image files (including SVG's) are run through the [imagemin](https://github.com/imagemin/imagemin) plugin to losslessly compress them.

## PostCSS Assets

[The PostCSS Assets](https://github.com/borodean/postcss-assets) is run as part of the build process and includes several helpful functions which can be used in Sass files to help working with image assets easier.

### **URL Resolution**

The `resolve` function is provided, which helps rewrite the url paths in CSS background image `url()` properties so they don't break when assets are copied and Sass files are compiled.

```scss
.footer-share__link--facebook {
  background-image: resolve('images/icons/share/share-facebook.png');
}
```

would be rewritten as a relative URL in the CSS to the same image in the `/assets/images/` folder:

```css
.footer-share__link--facebook {
  background-image: url(../../assets/images/share/share-facebook.png);
}
```

### **Image Dimensions**

`width`, `height`, and `size` functions are available to calculate the respective dimensions of an image so it can be used in CSS.

If the share-facebook.png image was 16px pixels square, then:

```scss
.footer-share__link--facebook {
  width: width('images/icons/share/share-facebook.png');
  height: height('images/icons/share/share-facebook.png');
}
```

would be generated into css as:

```css
.footer-share__link--facebook {
  width: 16px;
  height: 16px;
}
```

The `size` function gets both the height and width and combines them in a way suitable for background image sizes:

```scss
.footer-share__link--facebook {
  background-size: size('images/icons/share/share-facebook.png');
}
```

becomes

```css
.footer-share__link--facebook {
  background-size: 16px 16px;
}
```

You can compensate for HiDPI images by passing the resolution ratio as a second parameter in any of the image dimension functions:

```scss
.footer-share__link--facebook {
  // 32 x 32 pixel square image.
  background-size: size('images/icons/share/share-facebook@2x.png', 2);
}
```

becomes

```css
.footer-share__link--facebook {
  background-size: 16px 16px;
}
```
