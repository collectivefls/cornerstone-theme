# CI/CD

By default sub-theme's based off Cornerstone are assumed to have a CI/CD build styles for compiling front-end assets with the theme's [Gulp build system](/build-system/gulp.md).

To facilitate this the `package.json` file has some built-in NPM scripts and Gulp commands to run these build processes.

## Clean node modules folder

```shell
npm run clean
```

## Re-install all node modules

```shell
npm run re-install
```

## Compile all assets with production settings

```shell
gulp build --env=prod
```

## Lint Sass

```shell
npm run stylelint
```

## Lint JavaScript

```shell
npm run eslint
```

## Both Sass and JavaScript

You can lint both Sass and JavaScript in a single command as well:

```shell
npm run validate
```
