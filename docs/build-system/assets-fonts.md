# Fonts

For custom `@font-face` typefaces, most type foundries provide a JavaScript or CSS snippet to load the fonts from a CDN. While this is often preferred, custom fonts can also be placed in the `/src/fonts/` folder. Fonts should be placed in subfolders with the same name as the typeface.

## Examples

```shell
/src/fonts/Open Sans/open-sans-regular.woff
/src/fonts/Open Sans/open-sans-regular.woff2
/src/fonts/Open Sans/open-sans-bold.woff
/src/fonts/Open Sans/open-sans-bold.woff2
/src/fonts/Avenir Pro/avenir-pro-normal.woff
/src/fonts/Avenir Pro/avenir-pro-normal.woff2
/src/fonts/Avenir Pro/avenir-pro-italic.woff
/src/fonts/Avenir Pro/avenir-pro-italic.woff2
```

## @font-face Sass Mixin

To make loading these fonts easier, the Sass mixin library [font-face-generator](https://www.npmjs.com/package/font-face-generator) is provided as well. Extensive examples of using the mixin are provided on the [library's NPM page](https://www.npmjs.com/package/font-face-generator), but a simple example is:

```scss
@include font-face(
  $fonts: (
    'Open Sans': (
      400: 'open-sans-regular',
      600: 'open-sans-bold',
    ),
    'Avenir Pro': (
      400: (
        normal: 'avenir-pro-normal',
        italic: 'avenir-pro-italic',
      ),
    ),
  ),
  $types: 'woff' 'woff2'
);
```

The [Gulp.js](/build-system/gulp.md) build system automatically copies the fonts to the `/assets/fonts/` folder.
