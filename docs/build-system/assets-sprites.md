# Sprites

Cornerstone includes the PostCSS plugin [PostCSS Easysprites](https://github.com/glebmachine/postcss-easysprites). As the name implies, it makes generating image sprites extremely easy.

The plugin runs automatically as part of the regular asset Gulp.js build process. The only requirement is that the images used in the sprite as in the default `/src/images` folder (preferably organized in a sub-folder.)

The sprite images are outputted to the `/assets/images/sprites` folder. You can use the `resolve()` path function provided by the [PostCSS Assets](/build-system/assets-images.md) plugin to set the background image URL.

## Example

For an sprite sheet of social media icons, if the individual icons are in the `/src/images/icons/share/` folder the Sass would be something like:

```scss
.footer-share__link--facebook {
  background-image: resolve('images/icons/share/share-facebook.png#icons');
}

.footer-share__link--twitter {
  background-image: resolve('images/icons/share/share-twitter.png#icons');
}
```

A sprite image file named `icons.png` will then be generated to `/assets/images/sprites/icons.png` and the generated css would be:

```css
.footer-share__link--facebook {
  background-image: url(../../assets/images/sprites/icons.png);
  background-position: -164px -88px;
}

.footer-share__link--twitter {
  background-image: url(../../assets/images/sprites/icons.png);
  background-position: 0 -164px;
}
```

> [!WARNING] The plugin only generates sprites from images referenced in Sass files with the `#suffix` URL pattern. This is so you have control over what images are used as sprites and which aren't.

## Retina/HiDPI Sprites

Making HiDPI sprites with the plugin is easy as well. All you need to do is name the the HiDPI versions of the images for with an `@` suffix and the image resolution ratio number. The plugin will automatically detect this and create a HiDPI version of the sprite.

## HiDPI Example

If we had 2x versions of the icons in the previous example in the same `/src/images/icons/share/` folder, the additional Sass to add the HiDPI version of the sprite would be:

```scss
@include media('retina2x') {
  .footer-share__link--facebook {
    background-image: resolve('images/icons/share/share-facebook@2x.png#icons');
  }

  .footer-share__link--twitter {
    background-image: resolve('images/icons/share/share-twitter@2x.png#icons');
  }
}
```

> [!WARNING] Don't forget you still need to wrap the Sass in a Retina/HiDPI media query. It's easy with the [include-media](https://include-media.com) library though, since it has a built in `retina2x` query we can use.

and the generated CSS would be something like:

```css
@media (-webkit-min-device-pixel-ratio: 2),
  (min-resolution: 192dpi),
  (min-resolution: 2dppx) {
  .footer-share__link--facebook {
    background-image: url(../../assets/images/sprites/icons@2x.png);
    background-position: -40px -120px;
    background-size: 216px 174px;
  }
  .footer-share__link--twitter {
    background-image: url(../../assets/images/sprites/icons@2x.png);
    background-position: -80px -120px;
    background-size: 216px 174px;
  }
}
```

with a `icon@2x.png` version of the sprite being generated in the same `/assets/images/sprites/` folder.
