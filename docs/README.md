<!-- markdownlint-disable MD033 -->

# Cornerstone Theme

<div class="d-flex">
<img src="logo.svg" alt="Cornerstone Logo" class="homepage-logo">

> **cornerstone [kawr-ner-stohn]** something that is essential, indispensable, or basic: the chief foundation on which something is constructed or developed.

</div>

Cornerstone is a Drupal base theme designed for advanced Drupal themers. It provides a starting point for creating custom themes for companies and clients.

Because of its developer/advanced themer focus, it is not designed to be an out-of-the box turn-key theme. An advanced knowledge of Sass/CSS, JavaScript, Twig, and Drupal theming concepts, is assumed.
