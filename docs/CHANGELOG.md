## [0.4.4](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.4.3...0.4.4) (2019-10-03)

### Features

- **gulp:** Remove a11y and pattern lab tasks. ([7cdfc70](https://bitbucket.org/collectivefls/cornerstone-theme/commits/7cdfc70))
- **gulp:** Split gulp tasks into separate files. ([31f894e](https://bitbucket.org/collectivefls/cornerstone-theme/commits/31f894e)), closes [#2](https://bitbucket.org/collectivefls/cornerstone-theme/issue/2)
- **npm:** Add nvm use configuration file. ([705429d](https://bitbucket.org/collectivefls/cornerstone-theme/commits/705429d))

## [0.4.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.4.2...0.4.3) (2019-07-23)

### Bug Fixes

- **twig:** Remove aria attribute on hidden form inputs. ([c4ea5d6](https://bitbucket.org/collectivefls/cornerstone-theme/commits/c4ea5d6))

## [0.4.2](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.4.1...0.4.2) (2019-06-21)

### Bug Fixes

- **twig:** Add default views conditionals to twig template. ([efea48c](https://bitbucket.org/collectivefls/cornerstone-theme/commits/efea48c))
- **twig:** Fix template overrides for layout_builder compatability. ([c1e43f6](https://bitbucket.org/collectivefls/cornerstone-theme/commits/c1e43f6))

### BREAKING CHANGES

- **twig:** Template fixes might cause change of theme display if themes relied on previous markup/classes.

## [0.3.4](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.3...0.3.4) (2019-03-22)

### Features

- **gulp:** Separated out browsersync configuration into it's own file. ([de9feca](https://bitbucket.org/collectivefls/cornerstone-theme/commits/de9feca))
- **sass:** Cleaned up and renamed some sub-theme template sass files. ([90de43a](https://bitbucket.org/collectivefls/cornerstone-theme/commits/90de43a))
- **twig:** Updated pager and messages twig templates. ([02736da](https://bitbucket.org/collectivefls/cornerstone-theme/commits/02736da))

## [0.3.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.2...0.3.3) (2019-02-25)

### Bug Fixes

- **gulp:** Fixed moderizr asset dest path. ([a9f88cf](https://bitbucket.org/collectivefls/cornerstone-theme/commits/a9f88cf))

## [0.3.2](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.1...0.3.2) (2019-02-25)

### Bug Fixes

- Fixed docsify paths. ([ed8aea3](https://bitbucket.org/collectivefls/cornerstone-theme/commits/ed8aea3))

## [0.3.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.0...0.3.1) (2019-02-25)

# [0.3.0](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.3...0.3.0) (2019-02-25)

### Features

- **vscode:** Added addional vscode settings. ([e4d6a9e](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e4d6a9e))

## [0.2.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.2...0.2.3) (2019-02-25)

### Features

- Added commitlint to pre-commit hook. ([1030c32](https://bitbucket.org/collectivefls/cornerstone-theme/commits/1030c32))

## [0.2.2](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.1...0.2.2) (2019-02-25)

## [0.2.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.0...0.2.1) (2019-02-25)

# [0.2.0](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.1.1...0.2.0) (2019-02-25)

### Features

- Added initial theme documentation. ([338c6fd](https://bitbucket.org/collectivefls/cornerstone-theme/commits/338c6fd))

## [0.1.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.1.0...0.1.1) (2018-12-31)

# [0.1.0](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.6...0.1.0) (2018-12-31)

### Features

- Removed some outdated optional dependencies to reduce feature overlap. ([b62826d](https://bitbucket.org/collectivefls/cornerstone-theme/commits/b62826d))

### BREAKING CHANGES

- Updated Node and NPM requirements to latest LTS releases, remove some Sass libraries.

## [0.0.6](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.5...0.0.6) (2018-12-14)

### Bug Fixes

- Fixed subtheme placeholder value in package-lock.json. ([22539b6](https://bitbucket.org/collectivefls/cornerstone-theme/commits/22539b6))

### Features

- Add test build former to git ignore so it doesn't get into VCS. ([67af80e](https://bitbucket.org/collectivefls/cornerstone-theme/commits/67af80e))
- Added vscode settings to autoformat js. ([e88e90c](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e88e90c))
- Updated inquirer prompts with better checks. ([ae23c7c](https://bitbucket.org/collectivefls/cornerstone-theme/commits/ae23c7c))
- Upgrade gulp task and dependencies to work with Gulp v4. ([9661484](https://bitbucket.org/collectivefls/cornerstone-theme/commits/9661484))

## [0.0.5](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.4...0.0.5) (2018-12-07)

### Bug Fixes

- **lint:** Fixed PHPCS lint errors. ([8d369b8](https://bitbucket.org/collectivefls/cornerstone-theme/commits/8d369b8))

## [0.0.4](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.3...0.0.4) (2018-12-07)

### Bug Fixes

- **grid:** Fixed parent selector error in include-media flexbox grid. ([e7693af](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e7693af))

### Features

- **docs:** Added some much needed documentation. ([b098373](https://bitbucket.org/collectivefls/cornerstone-theme/commits/b098373))

## [0.0.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.2...0.0.3) (2018-12-07)

### Bug Fixes

- **config:** Fixed base theme name. ([e3ef409](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e3ef409))
- **config:** Fixed the destination directory for the subtheme so it goes to the custom folder. ([65075ff](https://bitbucket.org/collectivefls/cornerstone-theme/commits/65075ff))

### Features

- Updated base theme assets. ([7847703](https://bitbucket.org/collectivefls/cornerstone-theme/commits/7847703))

## [0.0.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/1df92a7...0.0.1) (2018-12-03)

### Features

- Added Commitizen to managed git commit logs. ([1df92a7](https://bitbucket.org/collectivefls/cornerstone-theme/commits/1df92a7))
- Added composer config file so it can be published on Packagist. ([ca39eec](https://bitbucket.org/collectivefls/cornerstone-theme/commits/ca39eec))
