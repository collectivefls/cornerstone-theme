<!-- markdownlint-disable MD041 -->

- [Introduction](/)

- Getting started

  - [Requirements](/getting-started/requirements.md)
  - [Installing theme](/getting-started/base-theme.md)
  - [Creating a sub-theme](/getting-started/create-sub-theme.md)

- Folder Structure

  - [General](/folder-structure/general-folder-structure.md)
  - [Sass](/folder-structure/sass-folder-structure.md)

- Asset Build System

  - [Gulp](/build-system/gulp.md)
  - [Included Libraries](/build-system/included-libraries.md)
  - [Sass](/build-system/assets-sass.md)
  - [JavaScript](/build-system/assets-javascript.md)
  - [Fonts](/build-system/assets-fonts.md)
  - [Images](/build-system/assets-images.md)
  - [Sprites](/build-system/assets-sprites.md)
    <!-- - [BrowserSync](/build-system/browsersync.md) -->
  - [CI/CD](/build-system/ci-cd.md)

- Editor Support

  - [VSCode](/editor-support/vscode.md)
  - [Linters](/editor-support/linters.md)

<!-- - Grid System -->

[Changelog](CHANGELOG.md)
