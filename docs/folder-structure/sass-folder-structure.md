# Sass Folder Structure

The Sass file/folder structure roughly follows the standard SMACSS system of organization recommended by Drupal.

> [!WARNING] Because it is important what order Sass libraries and settings are imported, the folders have been prefixed with a numeral to keep this order when globbed by [Gulp.js](https://gulpjs.com) tasks.

```shell
_partials/
  1-tools/
    1-1-libraries/
    1-2-functions/
    1-3-mixins/
    1-4-settings/
    _import.tools.scss
  2-base/
  3-layouts/
  4-components/
  5-utilities/
  6-vendor/
    include-media-columns/
    include-media-hidden-classes/
    include-media-visible-classes/
  _7-hotfix.scss
_print/
layouts/
pages/
ckeditor.styles.scss
SUBTHEME_ASSET-print.styles.scss
SUBTHEME_ASSET.styles.scss
```

| Folder/Filename | Description |
| --- | --- |
| `_partials` | The main Sass partials folder. This is where 95% of your Sass will be written. |
| `_partials/1-tools/` | Vendor libraries and custom Sass functions, mixins, and variables. Nothing in the imports folder generates CSS by default. |
| `_partials/1-tools/1-1-libraries` | Third party Sass libraries. Many of the library paths are to npm node modules to make updating them easier. |
| `_partials/1-tools/1-2-functions` | Any custom Sass functions. |
| `_partials/1-tools/1-3-mixins` | Any custom Sass mixins. |
| `_partials/1-tools/1-4-settings` | Custom Sass variables. Includes both global settings as well as component and layout variables. |
| `_partials/1-tools/_import.tools.scss` | The main import Sass partial file. |
| `_partials/2-base/` | Reset and/or normalize styles, global style definitions, and un-classed HTML elements (e.g. `h2`, `ul`) styling. |
| `_partials/3-layouts/` | Abstract layout and main site layout sections. Should primarily be used for object/positioning. Minimal to no styling should be applied. |
| `_partials/4-components/` | Reusable styled UI components. |
| `_partials/5-utilities/` | Helper/utility classes. Use sparingly as many are hard overrides using !important. | `_partials/6-vendor/` | Third party Sass libraries that generate CSS by default. This should only be used for vendor libraries which aren't available on npm, or need to be modified for a particular theme. |
| `_partials/6-vendor/include-media-columns/` | A include-media plugin to make generating responsive width classes easier. It is not imported by default, but can be by un-commenting it's import line in `SUBTHEME_ASSET.styles.scss` |
| `_partials/6-vendor/include-media-hidden-classes/` | A include-media plugin to make generating responsive hidden visibility utility classes easier. |
| `_partials/6-vendor/include-media-visible-classes/` | A include-media plugin to make generating responsive visibility utility classes easier. |
| `_partials/_7-hotfix.scss` | Emergency styling fixes. This file should be regularly refactored into new components/layouts, or replaced with existing ones. |
| `_print/` | Partials folder for print-only styles. As an alternative you can remove this folder and instead put print styles within a layout or component file with a print-only media query. To make the management of these styles consistent, you should do it one way or the other, not both. |
| `layouts/` | Standalone layouts that should be loaded conditionally or otherwise should not be included in the main combined CSS file. |
| `layouts/grid-layout/` | The flexbox bases grid system Cornerstone includes by default. |
| `pages/` | Standalone styles for pages or site sections with specific or complex needs. A default one for homepages is included, but can be removed if not used. |
| `ckeditor.styles.scss` | CSS styles which will be injected into the CKEditor WYSIWYG editor for a more realistic rendering of what content will look like. |
| `SUBTHEME_ASSET-print.styles.scss` | The compiled print-only styles. `SUBTHEME_ASSET` will be replaced by the sub-themes actual theme kebab-case name. |
| `SUBTHEME_ASSET.styles.scss` | The main compiled styles for the site. `SUBTHEME_ASSET` will be replaced by the sub-themes actual theme kebab-case name. |
