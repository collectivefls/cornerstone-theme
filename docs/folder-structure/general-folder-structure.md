# Sub-theme General Folder Structure

Once a sub-theme has been created, the generated folder structure by default is the following:

```shell
.vscode
config/
  install/
src/
  fonts/
  images/
  js/
  sass/
    _partials/
    _print/
    layouts/
    pages/
templates/
  layout/
  patterns/
.editorconfig
.eslintignore
.eslintrc.json
.gitignore
.prettierignore
.prettierrc.js
.stylelintignore
.stylelintrc.json
favicon.ico
gulp.config.js
gulpfile.js
logo.svg
package-lock.json
package.json
README.md
screenshot.png
SUBTHEME_MACHINE_NAME.breakpoints.yml
SUBTHEME_MACHINE_NAME.info.yml
SUBTHEME_MACHINE_NAME.layouts.yml
SUBTHEME_MACHINE_NAME.libraries.yml
SUBTHEME_MACHINE_NAME.theme
```

| Folder/Filename | Description |
| --- | --- |
| `.vscode` | This hidden folder contains some default settings and extension recommendations to make theming and general development better with Drupal. |
| `config/install` | The standard Drupal theme config folder for theme settings. |
| `src/` | The main source folder for front-end files such as Sass, JavaScript, and images. Items in this folder are built and optimized via Gulp tasks and then placed in an `/assets/` folder in the theme root directory. |
| `src/fonts/` | The source directory where any custom @font-face files should be placed. A gulp task currently just copies the files to a `/assets/fonts/` folder, but may be updated in the future with an optimization process included. |
| `src/images/` | The source directory where theme-related images should be placed. A gulp task losslessly optimizes the files with [imagemin](https://github.com/imagemin/imagemin) and then copies the files to the `/assets/images/` folder. |
| `src/js/` | Any theme-related JavaScript files should be placed in this directory. A gulp task minifies them before they are copied to the `/assets/js/` folder. |
| `src/sass/` | All Sass source files are located here. A gulp task compiles them into CSS before copying them to the `/assets/css` folder. See [Sass folder structure](/folder-structure/sass-folder-structure.md) for a more in-depth look at how the Sass is structured. |
| `templates/` | As with all Drupal themes, the `.twig` template files are placed here, organized into sub-folders for better organization. You can override a template by copying it (_not moving_) from the Cornerstone base theme template directory to the templates directory of the sub-theme. |
| `templates/layout/` | Contains a few basic Drupal templates for pages and regions. Remember to change the regions printed in the `page.html.twig` file if they are changed in the themes `.info` file. |
| `templates/patterns/` | Contains a sample Grid component for the contrib module [UI Patterns](https://www.drupal.org/project/ui_patterns). If you are not using the module, this can be removed at any time. |
| `.editorconfig` | Configuration file for [EditorConfig](https://editorconfig.org) to keep basic file formatting consistent. |
| `.eslintignore` | Ignore file for the [ESLint](https://eslint.org) to exclude some JavaScript files and folders from being linted. |
| `.eslintrc.json` | [ESLint](https://eslint.org) configuration file. Is used by both a Gulp linting task, as well as the VSCode ESLint plugin to validate JavaScript files for formatting consistency and to prevent common errors and bad practices. |
| `.gitignore` | The theme's git ignore file. By default this excludes the generated `/assets` folder, and `node_modules` folder. **Note:** You will have to remove the `/assets/` folder exclusion if you are not using a CI/CD process to compile the theming assets. |
| `.prettierignore` | [Prettier](https://prettier.io) ignore file to exclude some folders (such as `/node_modules`) and files from being processed by the code formatter. |
| `.prettierrc.js` | Configuration file for the [Prettier](https://prettier.io) JavaScript formatter. This formatter works in conjunction with [ESLint](https://eslint.org) to help maintain JavaScript code quality. |
| `.stylelintignore` | Ignore file for [Stylelint](https://stylelint.io) to exclude some generated or 3rd party Sass and CSS from being formatted. |
| `.stylelintrc.json` | Configuration file for [Stylelint](https://stylelint.io) to standardize the formatting of Sass files. |
| `favicon.ico` | A default favicon file. This should be replaced with a custom one which matches the clients logo. |
| `gulp.config.js` | The main [Gulp.js](https://gulpjs.com) custom configuration file for command flags for Gulp plugins used by the theme's build process. |
| `gulpfile.js` | [Gulp.js](https://gulpjs.com) configuration file with several front-end related build tasks defined. |
| `logo.svg` | A default logo for the theme. This should be replaced by the clients actual logo. |
| `package-lock.json` | The Node.js package lock file which defines the specific version of node modules to install. This should be committed to git so the same version of modules are being used across team members. |
| `package.json` | The main Node.js package configuration JSON file. Contains the theme's dev dependencies and some additional node scripts for linting files. |  |
| `README.md` | The theme readme file which has basic information about the theme. The documentation you are reading is more extensive. |
| `screenshot.png` | A basic theme screenshot which will be displayed in the `/admin/appearance` Drupal admin area. This should be replaced by a custom one which better identifies the sub-theme so it is easy to find on the `/admin/appearance` page. |
| `SUBTHEME_MACHINE_NAME.breakpoints.yml` | The sub-theme Drupal breakpoints configuration file. By default this contains a simple breakpoint for retina/HiDPI displays. `SUBTHEME_MACHINE_NAME` will be replaced by the sub-theme's actual machine name. |
| `SUBTHEME_MACHINE_NAME.info.yml` | The sub-theme Drupal .info file which contains basic information about the theme such as its name, description, region names, and global CSS files. `SUBTHEME_MACHINE_NAME` will be replaced by the sub-theme's actual machine name. |
| `SUBTHEME_MACHINE_NAME.layouts.yml` | A sub-theme Drupal layout discovery file. By default contains a basic landing page layout. Additional layouts can be added/removed as needed. `SUBTHEME_MACHINE_NAME` will be replaced by the sub-theme's actual machine name. |
| `SUBTHEME_MACHINE_NAME.libraries.yml` | The sub-theme Drupal asset library definition file. By default it contains a basic library for loading the global theme CSS. `SUBTHEME_MACHINE_NAME` will be replaced by the sub-theme's actual machine name. |
| `SUBTHEME_MACHINE_NAME.theme` | The sub-theme Drupal `.theme` file for adding PreProcess and form alter functions for the theme. Several of these functions are included, but commented out, to make using them easier when needed. The commented out functions can be removed at any time, they are mearly there as a convenience. `SUBTHEME_MACHINE_NAME` will be replaced by the sub-theme's actual machine name. |
