module.exports = {
  extends: ['@commitlint/config-conventional'],
  rules: {
    'scope-empty': [0, 'always'],
    'scope-enum': [
      2,
      'always',
      [
        'docsify',
        'gulp',
        'sass',
        'generator',
        'php',
        'javascript',
        'twig',
        'vscode',
        'design',
        'composer',
        'npm',
      ],
    ],
    'subject-full-stop': [2, 'always', '.'],
    'subject-case': [
      2,
      'never',
      ['snake-case', 'start-case', 'pascal-case', 'upper-case'],
    ],
  },
};
