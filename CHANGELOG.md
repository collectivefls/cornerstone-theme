# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.6.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/v0.6.0...v0.6.1) (2020-07-27)


### Bug Fixes

* **twig:** remove blocktabs contrib module twig template. ([67ef59c](https://bitbucket.org/collectivefls/cornerstone-theme/commit/67ef59c15524a22ff2e985fae1f5f0bfff86aabc))

## [0.6.0](https://bitbucket.org/collectivefls/cornerstone-theme/compare/v0.5.0...v0.6.0) (2020-06-26)


### ⚠ BREAKING CHANGES

* **twig:** Templates may generate markup with additional whitespace.

### Features

* **twig:** remove whitespace collapse in twig templates. ([c75e622](https://bitbucket.org/collectivefls/cornerstone-theme/commit/c75e622dc7c377826dce099f26639e92b3d9236a))
* **twig:** update twig templates with drupal 9 stable markup. ([40de253](https://bitbucket.org/collectivefls/cornerstone-theme/commit/40de25300599aa08283fa928ee102f4e45b4c791))


### Bug Fixes

* **twig:** adjust clean_class filter to run on entire image style name. ([bcfccea](https://bitbucket.org/collectivefls/cornerstone-theme/commit/bcfcceab41c88a6cd0e30546e268866ac23da723))

## 0.5.0 (2020-04-10)

### ⚠ BREAKING CHANGES

- **twig:** Template fixes might cause change of theme display if themes relied on previous markup/classes.
- Updated Node and NPM requirements to latest LTS releases, remove some Sass libraries.

### Features

- Add test build former to git ignore so it doesn't get into VCS. ([67af80e](https://bitbucket.org/collectivefls/cornerstone-theme/commit/67af80e2431856071da17538444b8f30da4713ba))
- Added Commitizen to managed git commit logs. ([1df92a7](https://bitbucket.org/collectivefls/cornerstone-theme/commit/1df92a764ecbe83bcd6d362d8ad8555e0917d4dc))
- Added commitlint to pre-commit hook. ([1030c32](https://bitbucket.org/collectivefls/cornerstone-theme/commit/1030c32023a6923c1ac594bebdc41ec015b5baae))
- Added composer config file so it can be published on Packagist. ([ca39eec](https://bitbucket.org/collectivefls/cornerstone-theme/commit/ca39eecfc9b8d8ae61aef57910dda00fdb785c7f))
- Added initial theme documentation. ([338c6fd](https://bitbucket.org/collectivefls/cornerstone-theme/commit/338c6fd9db5661def0874544bcd3177ab5683292))
- Added vscode settings to autoformat js. ([e88e90c](https://bitbucket.org/collectivefls/cornerstone-theme/commit/e88e90c59de6af14eca8e513405b178d2253db98))
- Removed some outdated optional dependencies to reduce feature overlap. ([b62826d](https://bitbucket.org/collectivefls/cornerstone-theme/commit/b62826d7cd01d46e5a29b371124f0b1211b51dfc))
- **docs:** Added some much needed documentation. ([b098373](https://bitbucket.org/collectivefls/cornerstone-theme/commit/b098373e13c4f07e95ae4c4b12b5ad4e7af8de22))
- **gulp:** Remove a11y and pattern lab tasks. ([7cdfc70](https://bitbucket.org/collectivefls/cornerstone-theme/commit/7cdfc70f3c418b797c42da55b23657f98e211444))
- **gulp:** Separated out browsersync configuration into it's own file. ([de9feca](https://bitbucket.org/collectivefls/cornerstone-theme/commit/de9feca10a70be7ece736c26f0e73aba84e3ec52))
- **gulp:** Split gulp tasks into separate files. ([31f894e](https://bitbucket.org/collectivefls/cornerstone-theme/commit/31f894e4dc1f00ee0cc360831d1aa2607141ac48)), closes [#2](https://bitbucket.org/collectivefls/cornerstone-theme/issues/2)
- **npm:** Add nvm use configuration file. ([705429d](https://bitbucket.org/collectivefls/cornerstone-theme/commit/705429d579fc05eb7dc63a707f92b2db8e702bcd))
- **sass:** Cleaned up and renamed some sub-theme template sass files. ([90de43a](https://bitbucket.org/collectivefls/cornerstone-theme/commit/90de43a0e803b6e7438f92b37784af1d41096f36))
- Updated base theme assets. ([7847703](https://bitbucket.org/collectivefls/cornerstone-theme/commit/7847703eb50dd9427bafc0c0c9633084c17c85f8))
- Updated inquirer prompts with better checks. ([ae23c7c](https://bitbucket.org/collectivefls/cornerstone-theme/commit/ae23c7c86441365f50291e5a9d22e1c3af614f37))
- **twig:** Updated pager and messages twig templates. ([02736da](https://bitbucket.org/collectivefls/cornerstone-theme/commit/02736da706fb14039fa4676073fd725834bc229a))
- **vscode:** Added addional vscode settings. ([e4d6a9e](https://bitbucket.org/collectivefls/cornerstone-theme/commit/e4d6a9e69d871a0d2109956a2546d734b934ecd6))
- Upgrade gulp task and dependencies to work with Gulp v4. ([9661484](https://bitbucket.org/collectivefls/cornerstone-theme/commit/9661484c3ea2bab1f9cb58637fcdb1ebca166a3a))

### Bug Fixes

- **config:** Fixed base theme name. ([e3ef409](https://bitbucket.org/collectivefls/cornerstone-theme/commit/e3ef4097116074d5ddf9473ac78478d9c6468418))
- Fixed docsify paths. ([ed8aea3](https://bitbucket.org/collectivefls/cornerstone-theme/commit/ed8aea3fa725a28b4d2eb4fe335793bfe309fe9f))
- **config:** Fixed the destination directory for the subtheme so it goes to the custom folder. ([65075ff](https://bitbucket.org/collectivefls/cornerstone-theme/commit/65075ff45bef1f673e5a2a068313d0d28dfee052))
- **grid:** Fixed parent selector error in include-media flexbox grid. ([e7693af](https://bitbucket.org/collectivefls/cornerstone-theme/commit/e7693afb7c8cc790c0334e8300b2fefb71b2cfd4))
- Fixed subtheme placeholder value in package-lock.json. ([22539b6](https://bitbucket.org/collectivefls/cornerstone-theme/commit/22539b6f6c786ff5c535e73f2dfd645f5abace91))
- **gulp:** Fixed moderizr asset dest path. ([a9f88cf](https://bitbucket.org/collectivefls/cornerstone-theme/commit/a9f88cf6229ded92f499d88f114524ee7a518c58))
- **lint:** Fixed PHPCS lint errors. ([8d369b8](https://bitbucket.org/collectivefls/cornerstone-theme/commit/8d369b8b3c7c78ebcc6e9d3ba9875a2bd33b2371))
- **twig:** Add default views conditionals to twig template. ([efea48c](https://bitbucket.org/collectivefls/cornerstone-theme/commit/efea48c757ca160730abbab71e3e9108878da290))
- **twig:** Fix template overrides for layout_builder compatability. ([c1e43f6](https://bitbucket.org/collectivefls/cornerstone-theme/commit/c1e43f6c13881e0bf1eef9182cc006ebd958ac63))
- **twig:** Remove aria attribute on hidden form inputs. ([c4ea5d6](https://bitbucket.org/collectivefls/cornerstone-theme/commit/c4ea5d6093b6d6e0a20fff8786c27aed5e65d4c7))
- **twig:** remove hardcoded h3 tags. ([c14c73e](https://bitbucket.org/collectivefls/cornerstone-theme/commit/c14c73ea6bb2088a83480c7b51cc393050e7cb59))

## [0.4.4](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.4.3...0.4.4) (2019-10-03)

### Features

- **gulp:** Remove a11y and pattern lab tasks. ([7cdfc70](https://bitbucket.org/collectivefls/cornerstone-theme/commits/7cdfc70))
- **gulp:** Split gulp tasks into separate files. ([31f894e](https://bitbucket.org/collectivefls/cornerstone-theme/commits/31f894e)), closes [#2](https://bitbucket.org/collectivefls/cornerstone-theme/issue/2)
- **npm:** Add nvm use configuration file. ([705429d](https://bitbucket.org/collectivefls/cornerstone-theme/commits/705429d))

## [0.4.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.4.2...0.4.3) (2019-07-23)

### Bug Fixes

- **twig:** Remove aria attribute on hidden form inputs. ([c4ea5d6](https://bitbucket.org/collectivefls/cornerstone-theme/commits/c4ea5d6))

## [0.4.2](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.4.1...0.4.2) (2019-06-21)

### Bug Fixes

- **twig:** Add default views conditionals to twig template. ([efea48c](https://bitbucket.org/collectivefls/cornerstone-theme/commits/efea48c))
- **twig:** Fix template overrides for layout_builder compatability. ([c1e43f6](https://bitbucket.org/collectivefls/cornerstone-theme/commits/c1e43f6))

### BREAKING CHANGES

- **twig:** Template fixes might cause change of theme display if themes relied on previous markup/classes.

## [0.3.4](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.3...0.3.4) (2019-03-22)

### Features

- **gulp:** Separated out browsersync configuration into it's own file. ([de9feca](https://bitbucket.org/collectivefls/cornerstone-theme/commits/de9feca))
- **sass:** Cleaned up and renamed some sub-theme template sass files. ([90de43a](https://bitbucket.org/collectivefls/cornerstone-theme/commits/90de43a))
- **twig:** Updated pager and messages twig templates. ([02736da](https://bitbucket.org/collectivefls/cornerstone-theme/commits/02736da))

## [0.3.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.2...0.3.3) (2019-02-25)

### Bug Fixes

- **gulp:** Fixed moderizr asset dest path. ([a9f88cf](https://bitbucket.org/collectivefls/cornerstone-theme/commits/a9f88cf))

## [0.3.2](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.1...0.3.2) (2019-02-25)

### Bug Fixes

- Fixed docsify paths. ([ed8aea3](https://bitbucket.org/collectivefls/cornerstone-theme/commits/ed8aea3))

## [0.3.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.3.0...0.3.1) (2019-02-25)

# [0.3.0](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.3...0.3.0) (2019-02-25)

### Features

- **vscode:** Added addional vscode settings. ([e4d6a9e](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e4d6a9e))

## [0.2.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.2...0.2.3) (2019-02-25)

### Features

- Added commitlint to pre-commit hook. ([1030c32](https://bitbucket.org/collectivefls/cornerstone-theme/commits/1030c32))

## [0.2.2](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.1...0.2.2) (2019-02-25)

## [0.2.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.2.0...0.2.1) (2019-02-25)

# [0.2.0](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.1.1...0.2.0) (2019-02-25)

### Features

- Added initial theme documentation. ([338c6fd](https://bitbucket.org/collectivefls/cornerstone-theme/commits/338c6fd))

## [0.1.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.1.0...0.1.1) (2018-12-31)

# [0.1.0](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.6...0.1.0) (2018-12-31)

### Features

- Removed some outdated optional dependencies to reduce feature overlap. ([b62826d](https://bitbucket.org/collectivefls/cornerstone-theme/commits/b62826d))

### BREAKING CHANGES

- Updated Node and NPM requirements to latest LTS releases, remove some Sass libraries.

## [0.0.6](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.5...0.0.6) (2018-12-14)

### Bug Fixes

- Fixed subtheme placeholder value in package-lock.json. ([22539b6](https://bitbucket.org/collectivefls/cornerstone-theme/commits/22539b6))

### Features

- Add test build former to git ignore so it doesn't get into VCS. ([67af80e](https://bitbucket.org/collectivefls/cornerstone-theme/commits/67af80e))
- Added vscode settings to autoformat js. ([e88e90c](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e88e90c))
- Updated inquirer prompts with better checks. ([ae23c7c](https://bitbucket.org/collectivefls/cornerstone-theme/commits/ae23c7c))
- Upgrade gulp task and dependencies to work with Gulp v4. ([9661484](https://bitbucket.org/collectivefls/cornerstone-theme/commits/9661484))

## [0.0.5](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.4...0.0.5) (2018-12-07)

### Bug Fixes

- **lint:** Fixed PHPCS lint errors. ([8d369b8](https://bitbucket.org/collectivefls/cornerstone-theme/commits/8d369b8))

## [0.0.4](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.3...0.0.4) (2018-12-07)

### Bug Fixes

- **grid:** Fixed parent selector error in include-media flexbox grid. ([e7693af](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e7693af))

### Features

- **docs:** Added some much needed documentation. ([b098373](https://bitbucket.org/collectivefls/cornerstone-theme/commits/b098373))

## [0.0.3](https://bitbucket.org/collectivefls/cornerstone-theme/compare/0.0.2...0.0.3) (2018-12-07)

### Bug Fixes

- **config:** Fixed base theme name. ([e3ef409](https://bitbucket.org/collectivefls/cornerstone-theme/commits/e3ef409))
- **config:** Fixed the destination directory for the subtheme so it goes to the custom folder. ([65075ff](https://bitbucket.org/collectivefls/cornerstone-theme/commits/65075ff))

### Features

- Updated base theme assets. ([7847703](https://bitbucket.org/collectivefls/cornerstone-theme/commits/7847703))

## [0.0.1](https://bitbucket.org/collectivefls/cornerstone-theme/compare/1df92a7...0.0.1) (2018-12-03)

### Features

- Added Commitizen to managed git commit logs. ([1df92a7](https://bitbucket.org/collectivefls/cornerstone-theme/commits/1df92a7))
- Added composer config file so it can be published on Packagist. ([ca39eec](https://bitbucket.org/collectivefls/cornerstone-theme/commits/ca39eec))
