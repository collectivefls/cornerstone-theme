'use strict';

const shell = require('shelljs');
const inquirer = require('inquirer');
const _ = require('lodash');

// An array of questions to ask users about their subtheme.
const questions = [
  {
    type: 'input',
    name: 'name',
    message: 'Sub-theme Name:',

    validate(value) {
      if (!_.trim(value).length) {
        return 'Sub-themes must have a name.';
      }

      return true;
    },
    filter: function(value) {
      return _.trim(value);
    },
  },
  {
    type: 'input',
    name: 'machine_name',
    message: 'Sub-theme Machine Name:',
    default: function(answers) {
      return _.snakeCase(answers.name);
    },
    validate(value) {
      if (!value.length) {
        return 'Sub-themes must have a machine name.';
      }

      const machine_name = _.snakeCase(value);

      if (value !== machine_name) {
        return 'Sub-theme machine names must be snake case.';
      }

      if (shell.test('-d', `../${machine_name}`)) {
        return 'A sub-theme with that machine name already exists.';
      }

      return true;
    },
    filter: function(value) {
      return _.snakeCase(value);
    },
  },
  {
    type: 'confirm',
    name: 'confirm',
    message: function(answers) {
      return `Create sub-theme with these values?\nName: ${answers.name}\nMachine name: ${answers.machine_name}\n`;
    },
    default: true,
  },
];

// Start the user prompts.
inquirer.prompt(questions).then((answers) => {
  const customThemeDir = '../../custom/';

  // Regex pattern for finding binary files plus SVG's for good measure.
  const noBinary = /.+(.svg|.jpg|.jpeg|.gif|.png|.ico)$/i;

  if (answers.confirm === true) {
    // Set the kebab case theme name to use for asset filenames, like CSS.
    answers.asset = _.kebabCase(answers.name);

    // Check if there is a custom theme directory already. If not, create it.
    if (!shell.test('-e', customThemeDir)) {
      shell.mkdir('-p', customThemeDir);
    }

    // Copy the starter theme directory to the custom theme directory and
    // rename it to the desired machine name.
    shell.cp(
      '-R',
      'SUBTHEME_MACHINE_NAME',
      `${customThemeDir}${answers.machine_name}`
    );

    // Move into the new sub-theme directory.
    shell.cd(`${customThemeDir}${answers.machine_name}`);

    // Rename all placeholder text instances with their desired names.
    shell.sed(
      '-i',
      'SUBTHEME_MACHINE_NAME',
      answers.machine_name,
      shell.ls('-RA', '**/*.*').filter((path) => {
        // Ignore any binary files so they don't get corrupted
        // by the `sed` command.
        return path.search(noBinary) !== 0;
      })
    );

    shell.sed(
      '-i',
      'SUBTHEME_NAME',
      answers.name,
      shell.ls('-RA', '**/*.*').filter((path) => {
        // Ignore any binary files so they don't get corrupted
        // by the `sed` command.
        return path.search(noBinary) !== 0;
      })
    );

    shell.sed(
      '-i',
      'SUBTHEME_ASSET',
      answers.asset,
      shell.ls('-RA', '**/*.*').filter((path) => {
        // Ignore any binary files so they don't get corrupted
        // by the `sed` command.
        return path.search(noBinary) !== 0;
      })
    );

    // Get all the files with placeholder values in their filename.
    let files = shell.find('.').filter(function(file) {
      return file.match(/SUBTHEME_/);
    });

    // Rename all files with placeholder values to their correct ones.
    files.forEach(function(file) {
      if (file.search('SUBTHEME_NAME') !== -1) {
        shell.mv(file, _.replace(file, 'SUBTHEME_NAME', answers.name));
      } else if (file.search('SUBTHEME_MACHINE_NAME') !== -1) {
        shell.mv(
          file,
          _.replace(file, 'SUBTHEME_MACHINE_NAME', answers.machine_name)
        );
      } else if (file.search('SUBTHEME_ASSET') !== -1) {
        shell.mv(file, _.replace(file, 'SUBTHEME_ASSET', answers.asset));
      }
    });

    // Get all the files with .txt placeholder file extension in their filename.
    files = shell.find('.').filter(function(file) {
      return file.match(/(.twig.txt|.yml.txt|.theme.txt)$/);
    });

    // Rename .txt files to their correct file extensions.
    files.forEach(function(file) {
      shell.mv(file, file.replace(/.(yml|twig|theme)(.txt)/, '.$1'));
    });

    // Let the user know the sub-theme was generated successfully.
    shell.echo(
      `Congratulations, your sub-theme "${answers.name}" has been generated!`
    );

    // Add option to install npm dependencies now.
    inquirer
      .prompt([
        {
          type: 'confirm',
          name: 'npm_install',
          message:
            'Do you want to install the sub-theme front-end node dependencies now?',
          default: true,
        },
      ])
      .then((answers) => {
        if (answers.npm_install === true) {
          shell.echo('Installing node dependencies now...');

          shell.exec('npm install');

          shell.echo(
            'All node dependencies have been installed!\nYou can start your theme development by running the command "gulp" in the sub-theme directory.\nHappy theming!\n(You can remove the node_modules folder in the base theme if you wish, as it was only used for generating the sub-theme.)'
          );
        } else {
          shell.echo('Happy theming!');
        }
      });
  } else {
    // If the user doesn't approve of the generator selections, cancel
    // the generator.
    shell.echo(
      'Sub-theme generation canceled!\nYou can re-run the generator with the command: `npm run subtheme`'
    );
  }
});
