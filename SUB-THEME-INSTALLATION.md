# Subtheme Instructions

Below are the manual steps needed to create a new subtheme based on the Cornerstone theme.

## Option 1: Subtheme generator

A subtheme generator CLI is provided with the base theme. It requires having [NodeJS](https://nodejs.org) installed.

1. In the the base theme directory run `npm install` from the command line to install the generators dependencies.
1. Run the command `npm run subtheme` to execute generator.
1. Follow the CLI prompts for the required subtheme information.

When the generator completes, the new subtheme will be in the `/themes/custom/` folder. If you need it in a different directory you will have to move it manually. After generator has finished, the node_modules folder in the base theme directory can be removed.

## Option 2: Manual string replacement

Because this involves a lot of string replacement in filenames and file contents, it's best to use a text editor or other application that has a bulk find/replace and find/rename feature. [VSCode](https://code.visualstudio.com/) and [NameChanger](https://mrrsoftware.com/namechanger) are two good ones.

1. Move the `SUBTHEME_MACHINE_NAME` folder to the `/themes/custom` directory and rename the folder to match the subthemes machine name you want.
1. Rename all instances of `SUBTHEME_MACHINE_NAME` in both file names and file contents with the lowercase name of your theme, with all spaces replaced with underscores. **Example:** `SUBTHEME_MACHINE_NAME => my_company`
1. Rename all instances of `SUBTHEME_NAME` in file contents with the regular name of your theme. **Example:** `SUBTHEME_NAME => My Company`
1. Rename all instances of `SUBTHEME_ASSET` in both file names and file contents with the lowercase name of your theme, with all spaces replaced with dashes. **Example:** `SUBTHEME_ASSET => my-company`
1. Rename all `.yml.txt` files to just `.yml`
1. Rename all `.twig.txt` files to just `.twig`
1. Rename all `.theme.txt` files to just `.theme`
1. Open the subthemes `.info.yml` file and make sure the base theme key is set to "cornerstone" **Example:** `base theme: cornerstone`
1. In the new subtheme directory, run the command `npm install` via the terminal to install the theme's front-end npm dependencies.
1. Clear all drush caches.
1. You theme should now show up in the Drupal "Appearance" admin section and can be enabled.
1. To start the theme build/watch task, runn `gulp` via the terminal in the subtheme directory.
