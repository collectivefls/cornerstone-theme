// Example Browsersync configuration file.
module.exports = {
  proxy: {
    target: 'http://local.YOUR_LOCAL_VM_DOMAIN.com',
    ws: true,
  },
  host: 'YOUR_LOCAL_VM_IP_ADDRESS',
  watchOptions: {
    usePolling: true,
  },
  port: 3000,
  notify: true,
  open: true,
  enable: true,
};
