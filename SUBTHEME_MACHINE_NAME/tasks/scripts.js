module.exports = function scripts() {
  'use strict';

  const config = require('./config');
  const { src, dest } = require('gulp');
  const terser = require('gulp-terser');
  const notify = require('gulp-notify');
  const babel = require('gulp-babel');
  const plumber = require('gulp-plumber');
  const flatmap = require('gulp-flatmap');
  const rename = require('gulp-rename');

  // Check if filename has the .es6 suffix.
  const es6RegEx = /.+es6\.js$/g;

  let stream = src(config.js.src, {
    sourcemaps: config.sourcemaps.enabled,
  });

  stream = stream.pipe(plumber());

  stream = stream.pipe(
    flatmap(function(stream, file) {
      if (file.path.match(es6RegEx) && config.babel.enabled) {
        stream = stream.pipe(
          rename(function(path) {
            path.basename = path.basename.replace('.es6', '');
          })
        );

        stream = stream.pipe(
          babel({
            presets: [config.babel.presets],
          })
        );
      }

      return stream;
    })
  );

  stream = stream.pipe(terser());

  stream = stream.pipe(
    dest(config.js.dest, { sourcemaps: config.js.sourcemaps })
  );

  if (config.notify.enabled) {
    stream = stream.pipe(
      notify({
        onLast: true,
        title: 'JavaScript',
        message: 'JavaScript has been minified.',
      })
    );
  }

  return stream;
};
