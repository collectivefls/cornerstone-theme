module.exports = function clean(done) {
  'use strict';

  const config = require('../gulp.config');
  const del = require('del');

  async () => {
    await del(config.clean.paths);
  };

  done();
};
