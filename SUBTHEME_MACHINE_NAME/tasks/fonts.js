module.exports = function fonts() {
  'use strict';

  const config = require('./config');
  const { src, dest, lastRun } = require('gulp');

  let stream = src(config.fonts.src, { since: lastRun(fonts) });

  return stream.pipe(dest(config.fonts.dest));
};
