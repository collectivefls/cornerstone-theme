module.exports = function styles(done) {
  'use strict';

  const config = require('./config');
  const { src, dest } = require('gulp');
  const postcss = require('gulp-postcss');
  const sass = require('gulp-sass');
  const notify = require('gulp-notify');
  const easysprite = require('postcss-easysprites');
  const flexboxFixer = require('postcss-flexbugs-fixes');
  const assets = require('postcss-assets');
  const autoprefixer = require('autoprefixer');
  const inlineSVG = require('postcss-inline-svg');
  const easingGradients = require('postcss-easing-gradients');
  const magicImporter = require('node-sass-magic-importer');
  const cssnano = require('cssnano');
  const sassOptions = Object.assign(config.sass.settings, {
    importer: magicImporter(),
  });

  let stream = src(config.sass.src, {
    sourcemaps: config.sourcemaps.enabled,
  })
    .pipe(sass(sassOptions))
    .pipe(
      postcss([
        flexboxFixer(),
        autoprefixer(config.postcss.autoprefixer),
        inlineSVG(),
        assets({
          relative: config.postcss.assets.relative,
          loadPaths: [config.postcss.assets.loadPaths],
        }),
        easysprite({
          imagePath: config.postcss.easysprite.imagePath,
          spritePath: config.postcss.easysprite.spritePath,
        }),
        easingGradients(config.easingGradients),
      ])
    );

  if (config.postcss.cssnano.enabled) {
    stream = stream.pipe(postcss([cssnano(config.postcss.cssnano.preset)]));
  }

  stream = stream.pipe(
    dest(config.sass.dest, { sourcemaps: config.sass.sourcemaps })
  );

  if (config.notify.enabled) {
    stream = stream.pipe(
      notify({
        onLast: true,
        title: 'Sass',
        message: 'Sass has been compiled and minified.',
      })
    );
  }

  return stream;
};
