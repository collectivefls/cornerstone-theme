'use strict';

const server = require('browser-sync');
const config = require('./config');

function browserSyncServe(done) {
  if (config.browsersync.enabled) {
    server.init(config.browsersync.watchFiles, config.browsersync);
  }

  done();
}

function browserSyncStream(done) {
  if (config.browsersync.enabled) {
    server.stream({ match: '**/*.css' });
  }

  done();
}

function browserSyncReload(done) {
  if (config.browsersync.enabled) {
    server.reload();
  }

  done();
}

exports.stream = browserSyncStream;
exports.serve = browserSyncServe;
exports.reload = browserSyncReload;
