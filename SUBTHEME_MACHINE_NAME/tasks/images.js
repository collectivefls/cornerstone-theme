module.exports = function images() {
  'use strict';

  const config = require('./config');
  const { src, dest, lastRun } = require('gulp');
  const imagemin = require('gulp-imagemin');
  const notify = require('gulp-notify');

  let stream = src(config.images.src, { since: lastRun(images) }).pipe(
    imagemin()
  );

  stream = stream.pipe(dest(config.images.dest));

  if (config.notify.enabled) {
    stream = stream.pipe(
      notify({
        onLast: true,
        title: 'Images',
        message: 'Images have been minified.',
      })
    );
  }

  return Promise.resolve(stream);
};
