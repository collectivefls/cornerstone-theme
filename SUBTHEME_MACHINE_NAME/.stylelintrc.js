module.exports = {
  extends: [
    // Use the Standard config as the base
    // https://github.com/stylelint/stylelint-config-standard
    'stylelint-config-standard',
    // Enforce a standard order for CSS properties
    // https://github.com/stormwarning/stylelint-config-recess-order
    'stylelint-config-recess-order',
    // Override rules that would interfere with Prettier
    // https://github.com/shannonmoeller/stylelint-config-prettier
    'stylelint-prettier/recommended',
  ],
  plugins: ['stylelint-scss'],
  ignoreFiles: [
    './src/sass/_partials/1-tools/1-1-libraries/**/*.scss',
    './src/sass/_partials/1-tools/1-2-functions/**/*.scss',
    './src/sass/_partials/1-tools/1-3-mixins/**/*.scss',
    './src/sass/_partials/6-vendor/**/*.scss',
    './**/*.css',
    './**/*.map',
    './node_modules/**/*.scss',
    './node_modules/**/*.css',
  ],
  rules: {
    'scss/at-else-empty-line-before': 'never',
    'scss/at-extend-no-missing-placeholder': true,
    'scss/at-if-closing-brace-newline-after': 'always-last-in-chain',
    'scss/at-rule-no-unknown': true,
    'scss/dollar-variable-colon-space-after': 'always',
    'scss/dollar-variable-colon-space-before': 'never',
    'scss/double-slash-comment-whitespace-inside': 'always',
    'scss/operator-no-newline-after': true,
    'scss/operator-no-newline-before': true,
    'scss/operator-no-unspaced': true,
    'scss/selector-no-redundant-nesting-selector': true,

    'no-descending-specificity': null,
    'string-quotes': 'single',
    'max-empty-lines': 2,
    'function-max-empty-lines': 2,
    'block-closing-brace-newline-after': [
      'always-multi-line',
      {
        ignoreAtRules: ['if', '@if', 'else', '@else'],
      },
    ],
    'declaration-colon-newline-after': null,
    'at-rule-no-unknown': null,
    'at-rule-empty-line-before': [
      'always',
      {
        except: ['blockless-after-same-name-blockless', 'first-nested'],
        ignore: ['after-comment'],
        ignoreAtRules: ['else', '@else'],
      },
    ],
    'function-calc-no-invalid': null,
  },
};
